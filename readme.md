Project Description

Recycler is an application that works with GPX files. It parses the files retrieving the routes, tracks and waypoints drawing them on the map.
It also provides elevation charts and a tracking of the GPX routes via Street View service.
The project uses the following services/libraries:

1. Firebase:
    o Firebase Authentication
        * Create new account
        * Login, Logout
        * Update profile
        * Reset password (by email)

    o Firebase Cloud Storage: store GPX files
        * Upload files
        * Download files

    o Firebase Realtime Database
        All users can see/download the files uploaded by all other users.
        Store information about GPX files:
            * Name
            * URL: where the GPX file can be downloaded
            * User name: user that uploaded the file
        Use this approach to list all the GPX files available in the cloud and to avoid downloading them until they are needed.
        When a file is selected from the list, it will be downloaded from the cloud.
        When a user uploads a file, the list of files of all users will be automatically updated.

2. Google API:
    o Drawing on the map: draw the routes, tracks, waypoints from the GPX files on a map using polylines
    o Street View: navigate through routes and tracks via Street View
    o Roads API: gets the best-fit road geometry for a given set of GPS coordinates (from our routes and tracks)

3. GPX Parser: We used an external GPX parser library to parse the GPX files and extract the routes, tracks and waypoints.

4. AChartEngine: We used AChartEngine library to draw elevation charts for routes and tracks from the GPX files.


Project setup

1. Firebase: You have to setup Firebase for the application. Follow the steps from https://firebase.google.com/docs/android/setup
   Warning: apply plugin: 'com.google.gms.google-services' will give compilation error because it does not find google-services.json, this file is obtained by setting Firebase
2. Google Maps API: You have to get an API key from https://developers.google.com/maps/documentation/android-api/signup
and set it into the AndroidManifest.xml file of the project.

TODO

* create google/firebase accounts

* update project to be able to work offline/ no Firebase (no login, no cloud support)

* right now we are moving on street view with a constant playback speed, make this speed dynamic, based on GPX data

* use GPX data to playback a 360 degree virb video, playback speed is controlled by the data obtained from the GPX file


