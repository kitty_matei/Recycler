package com.example.mateic.recycler;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXWayPoint;
import com.example.mateic.recycler.adapter.GPXWayPointAdapter;

import java.util.ArrayList;


public class WayPointFragment extends Fragment implements GPXWayPointAdapter.ItemClickListener {

    public static ArrayList<GPXWayPoint> sWayPoints;
    public static boolean isDownloaded = false;
    private RecyclerView mRecyclerView;
    private TextView mTextView;
    private ProgressBar mProgressBar;
    private GPXWayPointAdapter mAdapter = new GPXWayPointAdapter(this, sWayPoints);

    public void setWayPoints(ArrayList<GPXWayPoint> wayPoints) {
        sWayPoints = wayPoints;
        mAdapter.setWayPoints(wayPoints);
        isDownloaded = true;
        showWayPoints();
    }

    @Override
    public void onItemClick(int position, View view) {
        TextView sharedView = view.findViewById(R.id.tv_way_point_name);
        WayPointDetailActivity.sWayPoint = sWayPoints.get(position);
        Intent intent = new Intent(getContext(), WayPointDetailActivity.class);
        checkStartActivity(intent, sharedView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_way_point, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_way_points);
        mTextView = (TextView) view.findViewById(R.id.tv_no_way_points);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_gpx_way_points);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        if (isDownloaded) {
            showWayPoints();
        }
        return view;
    }

    private void checkStartActivity(Intent intent, TextView sharedView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(getActivity(), sharedView, sharedView.getTransitionName()).toBundle();
            startActivity(intent, bundle);
        } else {
            startActivity(intent);
        }
    }

    private void showWayPoints() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
        if (sWayPoints.size() > 0) {
            if (mRecyclerView != null) {
                mRecyclerView.setVisibility(View.VISIBLE);
            }
        } else {
            if (mTextView != null) {
                mTextView.setVisibility(View.VISIBLE);
            }
        }
    }
}
