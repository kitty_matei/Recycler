package com.example.mateic.recycler;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class NewAccountActivity extends AppCompatActivity {

    private EditText mEditTextName;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private EditText mEditTextRePassword;
    private FirebaseAuth mAuth;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void createAccount(View view) {
        final String name = mEditTextName.getText().toString();
        String email = mEditTextEmail.getText().toString();
        String password = mEditTextPassword.getText().toString();
        String rePassword = mEditTextRePassword.getText().toString();

        if (!validateForm(name, email, password, rePassword)) {
            return;
        } else {
            if (!validatePassword(password, rePassword)) {
                return;
            }
        }
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(name)
                                    .build();
                            if (user != null) {
                                user.updateProfile(profileUpdates)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Intent intent = new Intent(NewAccountActivity.this, MainActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                } else {
                                                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewAccountActivity.this);
                                                    dlgAlert.setMessage(getText(R.string.error_create_account));
                                                    dlgAlert.setPositiveButton(getText(R.string.ok), null);
                                                    dlgAlert.setCancelable(true);
                                                    dlgAlert.create().show();
                                                }
                                            }
                                        });
                            }
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e) {
                                mEditTextPassword.setError(getText(R.string.error_weak_password));
                                mEditTextRePassword.setError(getText(R.string.error_weak_password));
                            } catch (FirebaseAuthUserCollisionException e) {
                                mEditTextEmail.setError(getText(R.string.error_email_in_use));
                            } catch (Exception e) {
                                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewAccountActivity.this);
                                dlgAlert.setMessage(getText(R.string.error_create_account));
                                dlgAlert.setPositiveButton(getText(R.string.ok), null);
                                dlgAlert.setCancelable(true);
                                dlgAlert.create().show();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        mAuth = FirebaseAuth.getInstance();
        mEditTextName = (EditText) findViewById(R.id.et_name);
        mEditTextEmail = (EditText) findViewById(R.id.et_email);
        mEditTextPassword = (EditText) findViewById(R.id.et_password);
        mEditTextRePassword = (EditText) findViewById(R.id.et_re_password);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayShowTitleEnabled(false);
            ab.setDisplayShowHomeEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private boolean validateForm(String name, String email, String password, String rePassword) {
        boolean valid = true;
        if (TextUtils.isEmpty(name)) {
            mEditTextName.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextName.setError(null);
        }
        if (TextUtils.isEmpty(email)) {
            mEditTextEmail.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextEmail.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            mEditTextPassword.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextPassword.setError(null);
        }
        if (TextUtils.isEmpty(rePassword)) {
            mEditTextRePassword.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextRePassword.setError(null);
        }
        return valid;
    }

    private boolean validatePassword(String password, String rePassword) {
        boolean valid = true;
        if (!password.equals(rePassword)) {
            mEditTextRePassword.setError(getText(R.string.error_different_password));
            valid = false;
        } else {
            mEditTextRePassword.setError(null);
        }
        return valid;
    }
}
