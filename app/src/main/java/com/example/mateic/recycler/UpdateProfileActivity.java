package com.example.mateic.recycler;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;

    private EditText mEditTextName;
    private EditText mEditTextEmail;
    private CircleImageView mImage;
    private FirebaseUser user;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void selectPhoto(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }
        startActivityForResult(Intent.createChooser(intent, getText(R.string.select_picture)), PICK_IMAGE);
    }

    public void updateProfile(View view) {
        final String name = mEditTextName.getText().toString();
        String email = mEditTextEmail.getText().toString();
        if (!validateForm(name, email)) {
            return;
        }
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UpdateProfileActivity.this, getText(R.string.message_profile_updated), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        user.updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UpdateProfileActivity.this, getText(R.string.message_profile_updated), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_update);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(getText(R.string.update_profile));
        }

        mEditTextName = (EditText) findViewById(R.id.et_update_name);
        mEditTextEmail = (EditText) findViewById(R.id.et_update_email);
        mImage = (CircleImageView) findViewById(R.id.ib_photo);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri image = user.getPhotoUrl();
            mEditTextName.setText(name);
            mEditTextEmail.setText(email);
            mImage.setImageURI(image);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            final Uri imageUri = data.getData();
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(imageUri)
                    .build();

            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mImage.setImageURI(imageUri);
                                Toast.makeText(UpdateProfileActivity.this, getText(R.string.message_photo_updated), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(UpdateProfileActivity.this, getText(R.string.error_update_photo), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private boolean validateForm(String name, String email) {
        boolean valid = true;

        if (TextUtils.isEmpty(email)) {
            mEditTextEmail.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextEmail.setError(null);
        }

        if (TextUtils.isEmpty(name)) {
            mEditTextName.setError(getText(R.string.error_required));
            valid = false;
        } else {
            mEditTextName.setError(null);
        }

        return valid;
    }

}
