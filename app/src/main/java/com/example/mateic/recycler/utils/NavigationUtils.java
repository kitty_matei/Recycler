package com.example.mateic.recycler.utils;

import com.google.maps.model.SnappedPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mateic on 8/24/2017.
 */

public class NavigationUtils {

    public static double getBearing(SnappedPoint src, SnappedPoint dst) {
        double lat1 = deg2rad(src.location.lat);
        double lng1 = deg2rad(src.location.lng);
        double lat2 = deg2rad(dst.location.lat);
        double lng2 = deg2rad(dst.location.lng);
        double dLon = (lng2 - lng1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        double bearing = Math.toDegrees((Math.atan2(y, x)));
        bearing = (bearing + 360) % 360;
        return bearing;
    }

    public static double getDistance(SnappedPoint src, SnappedPoint dst) {
        double R = 6371;
        double lat1 = deg2rad(src.location.lat);
        double lng1 = deg2rad(src.location.lng);
        double lat2 = deg2rad(dst.location.lat);
        double lng2 = deg2rad(dst.location.lng);
        double dLat = lat1 - lat2;
        double dLon = lng1 - lng2;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    private static double deg2rad(double deg) {
        return deg * (Math.PI / 180);
    }

    public static List<SnappedPoint> getFilteredPoints(List<SnappedPoint> points, double minDist) {
        List<SnappedPoint> filteredPoints = new ArrayList<>();
        filteredPoints.add(points.get(0));
        for (SnappedPoint point : points) {
            double distance = getDistance(filteredPoints.get(filteredPoints.size() - 1), point);
            if (distance > minDist) {
                filteredPoints.add(point);
            }
        }
        return filteredPoints;
    }
}
