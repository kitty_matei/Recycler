package com.example.mateic.recycler;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXRoute;
import com.codebutchery.androidgpx.data.GPXRoutePoint;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;

public class RouteDetailActivity extends AppCompatActivity implements OnMapReadyCallback, AppBarLayout.OnOffsetChangedListener {

    public static GPXRoute sRoute;
    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private int mMaxScrollSize;
    private TextView mTextViewName;
    private TextView mTextViewDescription;
    private FloatingActionButton mFab1;
    private FloatingActionButton mFab2;
    private GraphicalView mChart;
    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

    public void openStreetView(View view) {
        StreetViewActivity.sPoints = getAllPoints();
        Intent intent = new Intent(this, StreetViewActivity.class);
        startActivity(intent);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        int currentScrollPercentage = (Math.abs(i)) * 100 / mMaxScrollSize;
        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            ViewCompat.animate(mFab1).scaleY(0).scaleX(0).start();
            ViewCompat.animate(mFab2).scaleY(1).scaleX(1).start();
        }
        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            ViewCompat.animate(mFab1).scaleY(1).scaleX(1).start();
            ViewCompat.animate(mFab2).scaleY(0).scaleX(0).start();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        PolylineOptions polylineOptions = new PolylineOptions();
        ArrayList<GPXRoutePoint> routePoints = sRoute.getRoutePoints();
        for (int i = 0; i < routePoints.size(); i++) {
            GPXRoutePoint routePoint = routePoints.get(i);
            LatLng point = new LatLng(routePoint.getLatitude(), routePoint.getLongitude());
            polylineOptions.add(point);
            b.include(point);
            polylineOptions.color(Color.RED);
            polylineOptions.width(5);
            googleMap.addPolyline(polylineOptions);
            if (i == 0) {
                googleMap.addMarker(new MarkerOptions().position(point).title(getString(R.string.start)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            }
            if (i == routePoints.size() - 1) {
                googleMap.addMarker(new MarkerOptions().position(point).title(getString(R.string.stop)));
            }
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(polylineOptions.getPoints().get(0)));
        LatLngBounds bounds = b.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 450, 450, 5);
        googleMap.animateCamera(cu);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);
        mTextViewName = (TextView) findViewById(R.id.tv_route_name);
        mTextViewDescription = (TextView) findViewById(R.id.tv_route_description);
        mFab1 = (FloatingActionButton) findViewById(R.id.fab1);
        mFab2 = (FloatingActionButton) findViewById(R.id.fab2);
        setupNameDescription();
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_route);
        fm.getMapAsync(this);
        setupActionBar();
        setupChart();
    }

    private void setupChart() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.chart_route);
        if (mChart == null) {
            if (!addData()) {
                layout.setVisibility(View.INVISIBLE);
                return;
            }
            initChart();
            mChart = ChartFactory.getLineChartView(this, mDataset, mRenderer);
            layout.addView(mChart);
        } else {
            mChart.repaint();
        }
    }

    private void initChart() {
        mRenderer.setLabelsTextSize(16);
        mRenderer.setBackgroundColor(getResources().getColorStateList(R.color.colorPrimary).getDefaultColor());
        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setMarginsColor(getResources().getColorStateList(R.color.colorPrimaryDark).getDefaultColor());
        mRenderer.setShowLegend(false);
        mRenderer.setChartTitleTextSize(32);
        mRenderer.setChartTitle(getString(R.string.elevation));
        mRenderer.setMargins(new int[]{40, 32, 8, 32});
        mRenderer.setPanEnabled(false, false);
        mRenderer.setZoomEnabled(false, false);
    }

    private boolean addData() {
        boolean hasElevation = true;
        ArrayList<GPXRoutePoint> points = sRoute.getRoutePoints();
        XYSeries currentSeries = new XYSeries("");
        mDataset.addSeries(currentSeries);
        XYSeriesRenderer currentRenderer = new XYSeriesRenderer();
        mRenderer.addSeriesRenderer(currentRenderer);
        currentRenderer.setColor(getResources().getColorStateList(R.color.colorSecondary).getDefaultColor());
        for (int i = 0; i < points.size(); i++) {
            GPXRoutePoint point = points.get(i);
            if (point.getElevation() != null) {
                currentSeries.add(i, point.getElevation());
            } else {
                hasElevation = false;
            }
        }
        return hasElevation;
    }

    private void setupActionBar() {
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_route_detail);
        myToolbar.setTitle(sRoute.getName());
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout_route);
        appBarLayout.addOnOffsetChangedListener(this);
    }

    private ArrayList<com.google.maps.model.LatLng> getAllPoints() {
        ArrayList<com.google.maps.model.LatLng> list = new ArrayList<>();
        for (GPXRoutePoint point : sRoute.getRoutePoints()) {
            com.google.maps.model.LatLng latLng = new com.google.maps.model.LatLng(point.getLatitude(), point.getLongitude());
            list.add(latLng);
        }
        return list;
    }

    private void setupNameDescription() {
        String name = sRoute.getName();
        String description = sRoute.getUserDescription();
        ImageView separator = (ImageView) findViewById(R.id.iv2_route);
        if (name == null || name.equals("")) {
            mTextViewName.setVisibility(View.GONE);
        } else {
            mTextViewName.setText(name);
        }
        if (description == null || description.equals("")) {
            mTextViewDescription.setVisibility(View.GONE);
            separator.setVisibility(View.GONE);
        } else {
            mTextViewDescription.setText(description);
        }
    }

}
