package com.example.mateic.recycler.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.mateic.recycler.GPXDetailActivity;
import com.example.mateic.recycler.MainActivity;
import com.example.mateic.recycler.R;
import com.example.mateic.recycler.data.GPXFile;
import com.example.mateic.recycler.utils.FirebaseDatabaseUtils;
import com.example.mateic.recycler.utils.FirebaseStorageUtils;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;


/**
 * Created by mateic on 8/10/2017.
 */

public class GPXIntentService extends IntentService implements FirebaseStorageUtils.UploadCompleteListener, FirebaseStorageUtils.DownloadCompleteListener {

    public static final String ACTION_ADD_GPX_FILE = "add-file";
    public static final String ACTION_DOWNLOAD_GPX_FILE = "download_file";
    public static final String EXTRA_URI = "extra-uri";
    public static final String EXTRA_NAME = "extra-name";
    public static final String EXTRA_FILE = "extra-file";

    private Context context;

    public GPXIntentService() {
        super("GPXIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_ADD_GPX_FILE)) {
            Uri uri = Uri.parse(intent.getStringExtra(EXTRA_URI));
            FirebaseStorageUtils.uploadFile(uri, context, this);
        } else {
            if (action.equals(ACTION_DOWNLOAD_GPX_FILE)) {
                String uri = intent.getStringExtra(EXTRA_URI);
                String name = intent.getStringExtra(EXTRA_NAME);
                FirebaseStorageUtils.downloadFile(uri, name, context, this);
            }
        }
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        context = getApplicationContext();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCompleteUpload(Uri uri) {
        GPXFile file = new GPXFile(uri.getLastPathSegment(), uri.toString(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        FirebaseDatabaseUtils.addFile(file);
        Toast.makeText(context, context.getResources().getText(R.string.message_file_uploaded), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailUpload() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.BROADCAST_INTENT_ACTION);
        sendBroadcast(intent);
    }

    @Override
    public void onCompleteDownload(File file) {
        Intent intent = new Intent();
        intent.setAction(GPXDetailActivity.BROADCAST_INTENT_ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_FILE, file);
        sendBroadcast(intent);
    }

    @Override
    public void onFailDownload() {
        Intent intent = new Intent();
        intent.setAction(GPXDetailActivity.BROADCAST_INTENT_ACTION_DOWNLOAD);
        sendBroadcast(intent);
    }
}
