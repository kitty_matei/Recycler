package com.example.mateic.recycler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mateic.recycler.R;

/**
 * Created by mateic on 8/8/2017.
 */

public class DrawerListAdapter extends ArrayAdapter<String> {

    private String[] mItems;
    private Context mContext;

    public DrawerListAdapter(String[] items, Context context) {
        super(context, R.layout.drawer_item, items);
        mContext = context;
        mItems = items;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drawer_item, viewGroup, false);
        }
        ((TextView) view.findViewById(R.id.tv_drawer)).setText(getItem(i));
        return view;
    }
}
