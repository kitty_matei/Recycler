package com.example.mateic.recycler;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXTrack;
import com.example.mateic.recycler.adapter.GPXTrackAdapter;

import java.util.ArrayList;


public class TrackFragment extends Fragment implements GPXTrackAdapter.ItemClickListener {

    public static ArrayList<GPXTrack> sTracks;
    public static boolean isDownloaded = false;
    private RecyclerView mRecyclerView;
    private TextView mTextView;
    private ProgressBar mProgressBar;
    private GPXTrackAdapter mAdapter = new GPXTrackAdapter(this, sTracks);

    public void setTracks(ArrayList<GPXTrack> tracks) {
        sTracks = tracks;
        mAdapter.setTracks(tracks);
        isDownloaded = true;
        showTracks();
    }

    @Override
    public void onItemClick(int position, View view) {
        TextView sharedView = view.findViewById(R.id.tv_track_name);
        Intent intent = new Intent(getContext(), TrackDetailActivity.class);
        TrackDetailActivity.sTrack = sTracks.get(position);
        checkStartActivity(intent, sharedView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_track, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_tracks);
        mTextView = (TextView) view.findViewById(R.id.tv_no_tracks);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_gpx_tracks);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        if (isDownloaded) {
            showTracks();
        }
        return view;
    }

    private void checkStartActivity(Intent intent, TextView sharedView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(getActivity(), sharedView, sharedView.getTransitionName()).toBundle();
            startActivity(intent, bundle);
        } else {
            startActivity(intent);
        }
    }

    private void showTracks() {

        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
        if (sTracks.size() > 0) {
            if (mRecyclerView != null) {
                mRecyclerView.setVisibility(View.VISIBLE);
            }
        } else {
            if (mTextView != null) {
                mTextView.setVisibility(View.VISIBLE);
            }
        }
    }

}
