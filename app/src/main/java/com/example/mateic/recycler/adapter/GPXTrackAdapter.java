package com.example.mateic.recycler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXTrack;
import com.example.mateic.recycler.R;

import java.util.ArrayList;

/**
 * Created by mateic on 8/16/2017.
 */

public class GPXTrackAdapter extends RecyclerView.Adapter<GPXTrackAdapter.TrackViewHolder> {

    private ItemClickListener mListener;
    private ArrayList<GPXTrack> mTracks;

    public GPXTrackAdapter(ItemClickListener listener, ArrayList<GPXTrack> tracks) {
        mListener = listener;
        mTracks = tracks;
    }

    public void setTracks(ArrayList<GPXTrack> tracks) {
        mTracks = tracks;
        notifyDataSetChanged();
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.item_file_track, parent, false);
        return new GPXTrackAdapter.TrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackViewHolder holder, int position) {
        holder.mName.setText(mTracks.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (mTracks != null) {
            return mTracks.size();
        }

        return 0;
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);
    }

    class TrackViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {

        TextView mName;

        TrackViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.tv_track_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onItemClick(position, view);
        }
    }
}
