package com.example.mateic.recycler.utils;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

/**
 * Created by mateic on 8/10/2017.
 */

public class FirebaseStorageUtils {

    private static FirebaseStorage storage = FirebaseStorage.getInstance();

    public interface UploadCompleteListener {
        void onCompleteUpload(Uri uri);

        void onFailUpload();
    }

    public interface DownloadCompleteListener {
        void onCompleteDownload(File file);

        void onFailDownload();
    }

    public static void uploadFile(final Uri uri, final Context context, final UploadCompleteListener listener) {

        StorageReference fileRef = storage.getReference().child(uri.getLastPathSegment());
        UploadTask uploadTask = fileRef.putFile(uri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                listener.onFailUpload();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUri = taskSnapshot.getDownloadUrl();
                listener.onCompleteUpload(downloadUri);
            }
        });
    }

    public static void downloadFile(String uri, String name, final Context context, final DownloadCompleteListener listener) {
        StorageReference httpsReference = storage.getReferenceFromUrl(uri);
        final File localFile = new File(context.getFilesDir(), name);
        httpsReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                listener.onCompleteDownload(localFile);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                listener.onFailDownload();
            }
        });
    }
}
