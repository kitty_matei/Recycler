package com.example.mateic.recycler.utils;

/**
 * Created by mateic on 8/24/2017.
 */

public class TimeUtils {

    public static int hoursToMillis(double hours) {
        return (int) (hours * 3600 * 1000);
    }
}
