package com.example.mateic.recycler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.codebutchery.androidgpx.data.GPXDocument;
import com.codebutchery.androidgpx.data.GPXRoute;
import com.codebutchery.androidgpx.data.GPXRoutePoint;
import com.codebutchery.androidgpx.data.GPXSegment;
import com.codebutchery.androidgpx.data.GPXTrack;
import com.codebutchery.androidgpx.data.GPXTrackPoint;
import com.codebutchery.androidgpx.data.GPXWayPoint;
import com.codebutchery.androidgpx.xml.GpxParser;
import com.codebutchery.androidgpx.xml.GpxParserHandler;
import com.example.mateic.recycler.adapter.ViewPagerAdapter;
import com.example.mateic.recycler.data.GPXFile;
import com.example.mateic.recycler.service.GPXIntentService;
import com.example.mateic.recycler.utils.FileCacheUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class GPXDetailActivity extends AppCompatActivity implements GpxParser.GpxParserListener, GpxParserHandler.GpxParserProgressListener {

    public static final String BROADCAST_INTENT_ACTION_DOWNLOAD = "DOWNLOAD-COMPLETE";
    public static final String EXTRA_NAME = "extra-name";
    public static final String EXTRA_URL = "extra-url";
    public static final String EXTRA_USER = "extra-user";

    public static GPXDocument sDocument;
    private DownloadReceiver mReceiver;
    private GPXFile mGPXFile;
    private TrackFragment mTrackFragment;
    private WayPointFragment mWayPointFragment;
    private RouteFragment mRouteFragment;


    public class DownloadReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.hasExtra(GPXIntentService.EXTRA_FILE)) {
                File file = (File) intent.getExtras().get(GPXIntentService.EXTRA_FILE);
                setupParser(file);
            } else {
                new AlertDialog.Builder(context)
                        .setTitle(context.getResources().getText(R.string.error))
                        .setMessage(context.getResources().getText(R.string.error_file_download))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                mTrackFragment.setTracks(new ArrayList<GPXTrack>());
                mWayPointFragment.setWayPoints(new ArrayList<GPXWayPoint>());
                mRouteFragment.setRoutes(new ArrayList<GPXRoute>());
            }
        }
    }

    @Override
    public void onGpxParseStarted() {
    }

    @Override
    public void onGpxParseCompleted(GPXDocument document) {
        sDocument = document;
        mTrackFragment.setTracks(document.getTracks());
        mWayPointFragment.setWayPoints(document.getWayPoints());
        mRouteFragment.setRoutes(document.getRoutes());
    }

    @Override
    public void onGpxParseError(String type, String message, int lineNumber, int columnNumber) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(getString(R.string.error_occured) + message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onGpxNewTrackParsed(int count, GPXTrack track) {
    }

    @Override
    public void onGpxNewRouteParsed(int count, GPXRoute track) {
    }

    @Override
    public void onGpxNewSegmentParsed(int count, GPXSegment segment) {
    }

    @Override
    public void onGpxNewTrackPointParsed(int count, GPXTrackPoint trackPoint) {
    }

    @Override
    public void onGpxNewRoutePointParsed(int count, GPXRoutePoint routePoint) {
    }

    @Override
    public void onGpxNewWayPointParsed(int count, GPXWayPoint wayPoint) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpxdetail);

        Intent intent = getIntent();
        String fileName = intent.getStringExtra(EXTRA_NAME);
        String url = intent.getStringExtra(EXTRA_URL);
        String user = intent.getStringExtra(EXTRA_USER);
        mGPXFile = new GPXFile(fileName, url, user);

        mTrackFragment = new TrackFragment();
        mWayPointFragment = new WayPointFragment();
        mRouteFragment = new RouteFragment();

        setupActionBar();
        setupViewPager();
        if (sDocument == null) {
            downloadGPXFile();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupReceiver();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }

    private void setupActionBar() {
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_detail);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }
    }

    private void setupReceiver() {
        IntentFilter filter = new IntentFilter(BROADCAST_INTENT_ACTION_DOWNLOAD);
        mReceiver = new DownloadReceiver();
        registerReceiver(mReceiver, filter);
    }

    private void setupParser(File file) {
        InputStream input = null;
        try {
            input = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        new GpxParser(input, this, this).parse();
    }

    private void setupViewPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mTrackFragment, getText(R.string.tracks).toString());
        adapter.addFragment(mWayPointFragment, getText(R.string.waypoints).toString());
        adapter.addFragment(mRouteFragment, getText(R.string.routes).toString());
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void downloadGPXFile() {
        if (FileCacheUtils.verifyFileCache(mGPXFile.fileName, this)) {
            File file = new File(this.getFilesDir(), mGPXFile.fileName);
            setupParser(file);
        } else {
            Intent i = new Intent(this, GPXIntentService.class);
            i.setAction(GPXIntentService.ACTION_DOWNLOAD_GPX_FILE);
            i.putExtra(GPXIntentService.EXTRA_URI, mGPXFile.url);
            i.putExtra(GPXIntentService.EXTRA_NAME, mGPXFile.fileName);
            startService(i);
        }
    }
}
