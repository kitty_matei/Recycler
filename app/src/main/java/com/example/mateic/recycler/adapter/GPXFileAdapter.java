package com.example.mateic.recycler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mateic.recycler.R;
import com.example.mateic.recycler.data.GPXFile;

import java.util.ArrayList;

/**
 * Created by mateic on 8/7/2017.
 */

public class GPXFileAdapter extends RecyclerView.Adapter<GPXFileAdapter.FileViewHolder> {

    private ArrayList<GPXFile> mFiles;
    private ItemClickListener mListener;

    public GPXFileAdapter(ItemClickListener listener) {
        mFiles = new ArrayList<>();
        mListener = listener;
    }

    public GPXFileAdapter(ArrayList<GPXFile> files, ItemClickListener listener) {
        mFiles = files;
        mListener = listener;
    }

    public void setFileList(ArrayList<GPXFile> files) {
        mFiles = files;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(GPXFile file);
    }

    @Override
    public GPXFileAdapter.FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.item_file_list, parent, false);
        return new FileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GPXFileAdapter.FileViewHolder holder, int position) {
        GPXFile file = mFiles.get(position);
        holder.mFileName.setText(file.fileName);
        holder.mFileUser.setText(file.user);
    }

    @Override
    public int getItemCount() {
        if (mFiles != null)
            return mFiles.size();
        return 0;
    }

    class FileViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {
        TextView mFileName;
        TextView mFileUser;

        public FileViewHolder(View itemView) {
            super(itemView);

            mFileName = (TextView) itemView.findViewById(R.id.tv_file_name);
            mFileUser = (TextView) itemView.findViewById(R.id.tv_file_user);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onItemClick(mFiles.get(position));
        }
    }
}
