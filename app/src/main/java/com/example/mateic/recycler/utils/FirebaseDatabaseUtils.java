package com.example.mateic.recycler.utils;

import com.example.mateic.recycler.data.GPXFile;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

/**
 * Created by mateic on 8/10/2017.
 */

public class FirebaseDatabaseUtils {

    private static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("files");

    public interface FilesListener {
        void onReceiveFiles(Map<String, Object> files);
    }

    public static void addFile(GPXFile file) {
        String fileId = mDatabase.push().getKey();
        mDatabase.child(fileId).setValue(file);
    }

    public static void setFilesListener(final FilesListener listener) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> files = (Map<String, Object>) dataSnapshot.getValue();
                listener.onReceiveFiles(files);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


}
