package com.example.mateic.recycler.data;

/**
 * Created by mateic on 8/11/2017.
 */

public class GPXFile {

    public static final String FILE_NAME = "fileName";
    public static final String FILE_URL = "url";
    public static final String FILE_USER = "user";

    public String fileName;
    public String url;
    public String user;

    public GPXFile() {

    }

    public GPXFile(String fileName, String url, String user) {
        this.fileName = fileName;
        this.url = url;
        this.user = user;
    }
}
