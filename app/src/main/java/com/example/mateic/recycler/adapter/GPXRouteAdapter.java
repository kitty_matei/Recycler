package com.example.mateic.recycler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXRoute;
import com.example.mateic.recycler.R;

import java.util.ArrayList;

/**
 * Created by mateic on 8/16/2017.
 */

public class GPXRouteAdapter extends RecyclerView.Adapter<GPXRouteAdapter.RouteViewHolder> {

    private GPXRouteAdapter.ItemClickListener mListener;
    private ArrayList<GPXRoute> mRoutes;

    public GPXRouteAdapter(GPXRouteAdapter.ItemClickListener listener, ArrayList<GPXRoute> routes) {
        mListener = listener;
        mRoutes = routes;
    }

    public void setRoutes(ArrayList<GPXRoute> routes) {
        mRoutes = routes;
        notifyDataSetChanged();
    }

    @Override
    public GPXRouteAdapter.RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.item_file_route, parent, false);
        return new GPXRouteAdapter.RouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GPXRouteAdapter.RouteViewHolder holder, int position) {
        holder.mName.setText(mRoutes.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (mRoutes != null) {
            return mRoutes.size();
        }
        return 0;
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);
    }

    class RouteViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {

        TextView mName;

        RouteViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.tv_route_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onItemClick(position, view);
        }
    }
}
