package com.example.mateic.recycler;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXRoute;
import com.example.mateic.recycler.adapter.GPXRouteAdapter;

import java.util.ArrayList;


public class RouteFragment extends Fragment implements GPXRouteAdapter.ItemClickListener {

    public static ArrayList<GPXRoute> sRoutes;
    public static boolean isDownloaded = false;
    private RecyclerView mRecyclerView;
    private TextView mTextView;
    private ProgressBar mProgressBar;
    private GPXRouteAdapter mAdapter = new GPXRouteAdapter(this, sRoutes);

    public void setRoutes(ArrayList<GPXRoute> routes) {
        sRoutes = routes;
        mAdapter.setRoutes(routes);
        isDownloaded = true;
        showRoutes();
    }

    @Override
    public void onItemClick(int position, View view) {
        TextView sharedView = view.findViewById(R.id.tv_route_name);
        RouteDetailActivity.sRoute = sRoutes.get(position);
        Intent intent = new Intent(getContext(), RouteDetailActivity.class);
        checkStartActivity(intent, sharedView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_routes);
        mTextView = (TextView) view.findViewById(R.id.tv_no_routes);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_gpx_routes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        if (isDownloaded) {
            showRoutes();
        }
        return view;
    }

    private void checkStartActivity(Intent intent, TextView sharedView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(getActivity(), sharedView, sharedView.getTransitionName()).toBundle();
            startActivity(intent, bundle);
        } else {
            startActivity(intent);
        }
    }

    private void showRoutes() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
        if (sRoutes.size() > 0) {
            if (mRecyclerView != null) {
                mRecyclerView.setVisibility(View.VISIBLE);
            }
        } else {
            if (mTextView != null) {
                mTextView.setVisibility(View.VISIBLE);
            }
        }
    }
}
