package com.example.mateic.recycler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXWayPoint;
import com.example.mateic.recycler.R;

import java.util.ArrayList;

/**
 * Created by mateic on 8/16/2017.
 */

public class GPXWayPointAdapter extends RecyclerView.Adapter<GPXWayPointAdapter.WayPointViewHolder> {

    private GPXWayPointAdapter.ItemClickListener mListener;
    private ArrayList<GPXWayPoint> mWayPoints;

    public GPXWayPointAdapter(GPXWayPointAdapter.ItemClickListener listener, ArrayList<GPXWayPoint> wayPoints) {
        mListener = listener;
        mWayPoints = wayPoints;
    }

    public void setWayPoints(ArrayList<GPXWayPoint> wayPoints) {
        mWayPoints = wayPoints;
        notifyDataSetChanged();
    }

    @Override
    public GPXWayPointAdapter.WayPointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context c = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(c);
        View view = inflater.inflate(R.layout.item_file_way_point, parent, false);
        return new GPXWayPointAdapter.WayPointViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GPXWayPointAdapter.WayPointViewHolder holder, int position) {
        holder.mName.setText(mWayPoints.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (mWayPoints != null) {
            return mWayPoints.size();
        }
        return 0;
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);
    }

    class WayPointViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {

        TextView mName;

        WayPointViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.tv_way_point_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onItemClick(position, view);
        }
    }
}
