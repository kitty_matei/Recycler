package com.example.mateic.recycler.utils;

import android.content.Context;

import java.io.File;

/**
 * Created by mateic on 8/11/2017.
 */

public class FileCacheUtils {

    public static boolean verifyFileCache(String name, Context context) {
        String[] fileNames = getAllFiles(context);
        boolean isFileInCache = false;
        for (String s : fileNames) {
            if (s.equals(name)) {
                isFileInCache = true;
            }
        }
        return isFileInCache;
    }

    private static String[] getAllFiles(Context context) {

        File dir = context.getFilesDir();
        File[] list = dir.listFiles();
        String[] fileNames = new String[list.length];
        for (int i = 0; i < list.length; i++) {
            fileNames[i] = list[i].getName();
        }
        return fileNames;
    }

}
