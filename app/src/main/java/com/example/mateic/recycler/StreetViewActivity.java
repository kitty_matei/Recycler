package com.example.mateic.recycler;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.example.mateic.recycler.utils.NavigationUtils;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.maps.GeoApiContext;
import com.google.maps.RoadsApi;
import com.google.maps.model.LatLng;
import com.google.maps.model.SnappedPoint;

import java.util.ArrayList;
import java.util.List;


public class StreetViewActivity extends AppCompatActivity implements OnStreetViewPanoramaReadyCallback {

    private static final int PAGE_SIZE_LIMIT = 100;
    private static final int PAGINATION_OVERLAP = 5;
    private static final int DEFAULT_CRUISE_SPEED = 30;//kph
    private static final int DEFAULT_TIME = 2000;//milliseconds
    private static final double MIN_DISTANCE = 0.01f;

    public static List<LatLng> sPoints;
    private GeoApiContext mContext;
    private int mCount = 0;
    private int mTime = DEFAULT_TIME;
    private int mSpeed = DEFAULT_CRUISE_SPEED;
    private List<SnappedPoint> mSnappedPoints;
    private StreetViewPanorama mStreetViewPanorama;
    private Handler mHandler = new Handler();
    private Runnable periodicMovement = new Runnable() {
        @Override
        public void run() {
            onMovePosition();
        }
    };
    private RoadsAsyncTask mAsyncTask;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mAsyncTask.cancel(true);
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        mStreetViewPanorama = streetViewPanorama;
        //streetViewPanorama.setPosition(new com.google.android.gms.maps.model.LatLng(sPoints.get(0).lat, sPoints.get(0).lng));
        mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(new StreetViewPanorama.OnStreetViewPanoramaChangeListener() {
            @Override
            public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
                if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
                    mHandler.postDelayed(periodicMovement, mTime);
                } else {
                    mHandler.postDelayed(periodicMovement, 100);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_street_view);
        StreetViewPanoramaFragment streetViewPanoramaFragment = (StreetViewPanoramaFragment) getFragmentManager().findFragmentById(R.id.street_view);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
        mContext = new GeoApiContext().setApiKey(getString(R.string.google_maps_web_services_key));
        mAsyncTask=new RoadsAsyncTask();
        mAsyncTask.execute();
    }

    private void onMovePosition() {
        if (mStreetViewPanorama != null) {
            if (mCount < mSnappedPoints.size() - 1) {
                SnappedPoint src = mSnappedPoints.get(mCount);
                SnappedPoint dst = mSnappedPoints.get(mCount + 1);
                double bearing = NavigationUtils.getBearing(src, dst);
                //double distance = NavigationUtils.getDistance(src, dst);
                //double timeH = distance / mSpeed;
                //mTime = TimeUtils.hoursToMillis(timeH);
                StreetViewPanoramaCamera camera =
                        new StreetViewPanoramaCamera.Builder()
                                .zoom(mStreetViewPanorama.getPanoramaCamera().zoom)
                                .tilt(mStreetViewPanorama.getPanoramaCamera().tilt)
                                .bearing((float) bearing)
                                .build();
                mStreetViewPanorama.animateTo(camera, 1000);
                mStreetViewPanorama.setPosition(new com.google.android.gms.maps.model.LatLng(dst.location.lat, dst.location.lng));
                mCount++;
            } else {
                finish();
            }
        }
    }


    private class RoadsAsyncTask extends AsyncTask<Void, Void, List<SnappedPoint>> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected List<SnappedPoint> doInBackground(Void... params) {
            try {
                return snapToRoads(mContext);
            } catch (final Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<SnappedPoint> snappedPoints) {
            if (snappedPoints != null) {
                mSnappedPoints = snappedPoints;
                mHandler.post(periodicMovement);
            } else {
                new AlertDialog.Builder(StreetViewActivity.this)
                        .setTitle(getString(R.string.error))
                        .setMessage(getString(R.string.error_occured) + "Can't open Street View")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                onBackPressed();
                            }
                        })
                        .show();
            }
        }

        private List<SnappedPoint> snapToRoads(GeoApiContext context) throws Exception {
            List<SnappedPoint> snappedPoints = new ArrayList<>();
            int offset = 0;
            while (offset < sPoints.size()) {
                if (offset > 0) {
                    offset -= PAGINATION_OVERLAP;
                }
                int lowerBound = offset;
                int upperBound = Math.min(offset + PAGE_SIZE_LIMIT, sPoints.size());
                LatLng[] page = sPoints
                        .subList(lowerBound, upperBound)
                        .toArray(new LatLng[upperBound - lowerBound]);
                SnappedPoint[] points = RoadsApi.snapToRoads(context, true, page).await();
                boolean passedOverlap = false;
                for (SnappedPoint point : points) {
                    if (offset == 0 || point.originalIndex >= PAGINATION_OVERLAP) {
                        passedOverlap = true;
                    }
                    if (passedOverlap) {
                        snappedPoints.add(point);
                    }
                }
                offset = upperBound;
            }
            return NavigationUtils.getFilteredPoints(snappedPoints, MIN_DISTANCE);
        }
    }

}
