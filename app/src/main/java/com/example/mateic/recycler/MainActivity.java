package com.example.mateic.recycler;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mateic.recycler.adapter.DrawerListAdapter;
import com.example.mateic.recycler.adapter.GPXFileAdapter;
import com.example.mateic.recycler.data.GPXFile;
import com.example.mateic.recycler.service.GPXIntentService;
import com.example.mateic.recycler.utils.FirebaseDatabaseUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements FirebaseDatabaseUtils.FilesListener, GPXFileAdapter.ItemClickListener {

    public static final String BROADCAST_INTENT_ACTION = "ERROR-UPLOADING";
    private static final int POSITION_UPDATE = 0;
    private static final int POSITION_LOGOUT = 1;
    private static final int FILE_SELECT_CODE = 0;

    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    private Toolbar mToolbar;
    private ActionBar mActionBar;
    private CircleImageView mImage;
    private TextView mTextViewName;
    private TextView mTextViewEmail;
    private RecyclerView mRecyclerView;
    private GPXFileAdapter mAdapter;
    private ProgressBar mProgressBar;
    private FloatingActionButton mFloatingButton;
    private ErrorReceiver mReceiver;

    public class ErrorReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            showRecyclerView();
            new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getText(R.string.error))
                    .setMessage(context.getResources().getText(R.string.error_file_upload))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addGPXFile(View view) {
        Intent intent = new Intent();
        intent.setType("text/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, FILE_SELECT_CODE);
    }

    @Override
    public void onReceiveFiles(Map<String, Object> files) {
        showRecyclerView();
        if (files != null) {
            ArrayList<GPXFile> fileArrayList = new ArrayList<>();
            for (Map.Entry<String, Object> entry : files.entrySet()) {
                Map fileMap = (Map) entry.getValue();
                GPXFile file = new GPXFile((String) fileMap.get(GPXFile.FILE_NAME), (String) fileMap.get(GPXFile.FILE_URL), (String) fileMap.get(GPXFile.FILE_USER));
                fileArrayList.add(file);
            }
            mAdapter.setFileList(fileArrayList);
        }
    }

    @Override
    public void onItemClick(GPXFile file) {
        GPXDetailActivity.sDocument = null;
        TrackFragment.sTracks = new ArrayList<>();
        TrackFragment.isDownloaded = false;
        WayPointFragment.sWayPoints = new ArrayList<>();
        WayPointFragment.isDownloaded = false;
        RouteFragment.sRoutes = new ArrayList<>();
        RouteFragment.isDownloaded = false;
        Intent intent = new Intent(this, GPXDetailActivity.class);
        intent.putExtra(GPXDetailActivity.EXTRA_NAME, file.fileName);
        intent.putExtra(GPXDetailActivity.EXTRA_URL, file.url);
        intent.putExtra(GPXDetailActivity.EXTRA_USER, file.user);
        checkStartActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFloatingButton = (FloatingActionButton) findViewById(R.id.fab);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mTextViewName = (TextView) findViewById(R.id.tv_user_name);
        mTextViewEmail = (TextView) findViewById(R.id.tv_user_email);
        mImage = (CircleImageView) findViewById(R.id.iv_user_photo);

        setupActionBar();
        setupRecyclerView();
        setupDrawer();
        FirebaseDatabaseUtils.setFilesListener(this);
        if (isStoragePermissionGranted()) {
            mFloatingButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupHeader();
        setupReceiver();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FILE_SELECT_CODE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            showProgress();
            Uri uri = data.getData();
            String[] splitName = uri.getLastPathSegment().split("\\.");
            if (splitName.length > 1) {
                if (splitName[1].equals("gpx")) {
                    String[] splitPath = uri.toString().split(Environment.getExternalStorageDirectory().toString());
                    File file;
                    if (splitPath.length > 1) {
                        file = new File(Environment.getExternalStorageDirectory() + "/" + splitPath[1]);
                    } else {
                        file = new File(uri.toString());
                    }
                    Uri newUri = Uri.fromFile(file);
                    Intent intent = new Intent(this, GPXIntentService.class);
                    intent.setAction(GPXIntentService.ACTION_ADD_GPX_FILE);
                    intent.putExtra(GPXIntentService.EXTRA_URI, newUri.toString());
                    startService(intent);
                } else {
                    showRecyclerView();
                    new AlertDialog.Builder(this)
                            .setTitle(getText(R.string.error))
                            .setMessage(getString(R.string.error_gpx_file))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            } else {
                showRecyclerView();
                new AlertDialog.Builder(this)
                        .setTitle(getText(R.string.error))
                        .setMessage(getString(R.string.error_gpx_file))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
        }
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_files);
        mAdapter = new GPXFileAdapter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_view);
        mActivityTitle = getTitle().toString();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mActionBar.setTitle(getText(R.string.profile));
                invalidateOptionsMenu();
                mDrawerLayout.bringToFront();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                mActionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        String[] items = getResources().getStringArray(R.array.drawer_items);
        DrawerListAdapter adapter = new DrawerListAdapter(items, this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case POSITION_UPDATE: {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        Intent intent = new Intent(MainActivity.this, UpdateProfileActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case POSITION_LOGOUT: {
                        logOut();
                        break;
                    }
                }
            }
        });
        setupHeader();
    }

    private void setupHeader() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Uri imageUri = user.getPhotoUrl();
            String name = user.getDisplayName();
            String email = user.getEmail();
            mTextViewName.setText(name);
            mTextViewEmail.setText(email);
            mImage.setImageURI(imageUri);
        }
    }

    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showRecyclerView() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void logOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void checkStartActivity(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(this).toBundle();
            startActivity(intent, bundle);
        } else {
            startActivity(intent);
        }
    }

    private void setupReceiver() {
        IntentFilter filter = new IntentFilter(BROADCAST_INTENT_ACTION);
        mReceiver = new ErrorReceiver();
        registerReceiver(mReceiver, filter);
    }

    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mFloatingButton.setVisibility(View.VISIBLE);
        }
    }
}
