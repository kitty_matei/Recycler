package com.example.mateic.recycler;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.codebutchery.androidgpx.data.GPXWayPoint;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class WayPointDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static GPXWayPoint sWayPoint;
    private TextView mTextViewName;
    private TextView mTextViewDescription;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        LatLng point = new LatLng(sWayPoint.getLatitude(), sWayPoint.getLongitude());
        b.include(point);
        googleMap.addMarker(new MarkerOptions().position(point).title(sWayPoint.getName()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        LatLngBounds bounds = b.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 450, 450, 5);
        googleMap.animateCamera(cu);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_point_detail);
        mTextViewName = (TextView) findViewById(R.id.tv_way_point_name);
        mTextViewDescription = (TextView) findViewById(R.id.tv_way_point_description);
        setupNameDescription();
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_way_point);
        fm.getMapAsync(this);
        setupActionBar();
    }

    private void setupActionBar() {
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_way_point_detail);
        myToolbar.setTitle(sWayPoint.getName());
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }
    }

    private void setupNameDescription() {
        String name = sWayPoint.getName();
        String description = sWayPoint.getDescription();
        if (name == null || name.equals("")) {
            mTextViewName.setVisibility(View.GONE);
        } else {
            mTextViewName.setText(name);
        }
        if (description == null || description.equals("")) {
            mTextViewDescription.setVisibility(View.GONE);
        } else {
            mTextViewDescription.setText(description);
        }
    }
}
